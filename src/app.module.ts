import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { SensorModule } from './sensor/sensor.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Transport, ClientsModule } from '@nestjs/microservices';

@Module({
  imports: [SensorModule,
    MongooseModule.forRoot(
      'mongodb://admin:admin123@localhost:27017,localhost:27018,localhost:27019',
      {
       dbName: 'acu-sense'
      }
    )],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
