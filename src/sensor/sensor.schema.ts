import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type SensorDocument = Sensor & Document;

@Schema()
export class Sensor {
  @Prop()
  gatewayMacAddress: string;

  @Prop()
  updateDatetime: number = Date.now();

  @Prop()
  measurementDatetime: number;

  @Prop()
  rawData: string;

  @Prop()
  index: number;
}

export const SensorSchema = SchemaFactory.createForClass(Sensor);