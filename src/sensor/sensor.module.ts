import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SensorService } from './sensor.service';
import { SensorController } from './sensor.controller';
import { Sensor, SensorSchema } from './sensor.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: Sensor.name, schema: SensorSchema }])],
  controllers: [SensorController],
  providers: [SensorService]
})
export class SensorModule {}
