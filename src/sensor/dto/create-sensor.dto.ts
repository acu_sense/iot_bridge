export class CreateSensorDto {
    gatewayMacAddress: string;
    updateDatetime: number;
    measurementDatetime: number;
    rawData: string;
    index: number;
}
