import { Controller } from '@nestjs/common';
import { MessagePattern, Payload, Ctx, MqttContext } from '@nestjs/microservices';
import { SensorService } from './sensor.service';
import { CreateSensorDto } from './dto/create-sensor.dto';
import { SensorSchema } from './sensor.schema';

@Controller()
export class SensorController {
  constructor(private readonly sensorService: SensorService) {}
  @MessagePattern('sensor/+')
  create(@Payload() createSensorDto: CreateSensorDto) {
    // console.log(`Received data from MQTT: ${JSON.stringify(createSensorDto)}`)
    // console.log(`Received data from MQTT: ${createSensorDto.index}`)
    // var result = this.sensorService.create(createSensorDto);
    // console.log('Save sensor data to MongoDB successed!')
    // console.log(result)
    // console.log('-------------------')
    return this.sensorService.create(createSensorDto);
  }
}
