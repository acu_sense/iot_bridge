import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Sensor, SensorDocument } from './sensor.schema';

import { CreateSensorDto } from './dto/create-sensor.dto';
import { UpdateSensorDto } from './dto/update-sensor.dto';

@Injectable()
export class SensorService {
  constructor(@InjectModel(Sensor.name) private sensorModel: Model<SensorDocument>) {}

  async create(createSensorDto: CreateSensorDto): Promise<Sensor> {
    const newData: Sensor = {
      measurementDatetime : createSensorDto.measurementDatetime,
      index : createSensorDto.index,
      gatewayMacAddress: createSensorDto.gatewayMacAddress,
      rawData: createSensorDto.rawData,
      updateDatetime: Date.now()
    };
    const createSensor = new this.sensorModel(newData);
    return createSensor.save();
  }

  async createMany(createSensorDtos: CreateSensorDto[]): Promise<Sensor[]> {
    try{
      const newSensors = await this.sensorModel.insertMany(createSensorDtos);
      return newSensors;
    }
    catch{
      return Promise.reject();
    }
  }
}
